import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_image(query):
    """Get the url of a picture from the Pexels API"""
    url = f"https://api.pexels.com/v1/search?query={query}"
    
    headers = {"Authorization": PEXELS_API_KEY}
    
    response = requests.get(url, headers=headers)
    api_dic = response.json()

    return api_dic["photos"][0]["src"]["original"]

# def get_location_image(city, state):
#     url = "https://api.pexels.com/v1/search"
#     query = {"query": f'{city}{state}'}
#     header = {"Authorization": PEXELS_API_KEY}
    # response = reqeusts.get(url,params=query,headers=headers)
    # content = response.json()
    # return content["photos"][0]["src"]["original"]
#
    
    # headers = {"Authorization": PEXELS_API_KEY}
    # payload = {"query": city}  #what is being passed into the pexels website
    # # Set AUTH HEADER
    
    # # Get the api response
    # response = requests.get("https://api.pexels.com/v1/search", headers=headers, params=payload)
    # # turn into python dict
    # content = json.loads(response.content)

    # # Create a dictionary of data to use containing
    # #    picture
    # picture = {
    #     "picture_url": content["photos"][0]["src"]["original"]
    # }

    # # Return the dictionary
    # return picture


# def show_weather(city):
#     """GET URL FROM GEOCODE"""
    
#     url = f"http://api.openweathermap.org/geo/1.0/direct?q={city}&appid={OPEN_WEATHER_API_KEY}"
#     response = requests.get(url)
#     api_dic = response.json()
#     latcoords = api_dic[0]["lat"]
#     loncoords = api_dic[0]["lon"]

#     url_two = f"https://api.openweathermap.org/data/2.5/weather?lat={latcoords}&lon={loncoords}&appid={OPEN_WEATHER_API_KEY}"

#     response_two = requests.get(url_two)
#     weather_api = response_two.json()
#     temp = weather_api["main"]["temp"]
#     descript = weather_api["weather"][0]["description"]
#     weather = {
#         "temp": temp,
#         "descript": descript
#     }
#     return weather

def get_weather_data(city, state):
    
    url_one = "http://api.openweathermap.org/geo/1.0/direct"
    #set the parameters for the LON and LAT request
    params = {
        "q": f"{city},{state},US", # Query includes city, state, and country code (US)
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,#Adds the API_KEY to the end of your url with it being equal to appid
    }
    response = requests.get(url_one, params=params)#This puts the two things the url and the params together, the params will be added in the order they were put in
    content = response.json()#turns all the content that requests just grabbed into to usable python
    #Must use a try and except statement incase you cannot get a lat and lon from the data we pulled, this will make it so weather returns null instead of yellow page of sadness
    try:
        latitutde = content[0]["lat"]#grabs the lat from the new list stored in content
        longitude = content[0]["lon"]#grabs the lon from the new list stored in content
    except (KeyError, IndexError):
        return None
#now we set new params for our second link to use, so it can grab the correct information that we want
    params = {
        "lat": latitutde,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url_two = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url_two, params=params)#adds the second url with the new params
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }#we then return a dictionary that has the description and the temperature in it.

